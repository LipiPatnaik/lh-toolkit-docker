## Docker image for [LibreHealth Toolkit](https://librehealth.io/projects/lh-toolkit/)
[![pipeline status](https://gitlab.com/librehealth/toolkit/lh-toolkit-docker/badges/master/pipeline.svg)](https://gitlab.com/librehealth/toolkit/lh-toolkit-docker/commits/master)


The repository hosts docker compose and resource files required to build docker
images and start containers that can be used by users and developers to test the
latest code in the repository. The Docker container that is created uses standard
tomcat7:jre8 and mysql:5.7 images. It includes the following:
 - the legacy-ui module
 - webservices.rest module
 - the owa module
 - demo database for Toolkit 2.1 SNAPSHOT

The tomcat:7-jre8 uses OpenJDK 8, which in turn uses Debian 8.1 (Jessie) base
image. Thus the lh-toolkit is deployed on tomcat7. The MySQL 5.7 image is used
for the database on which the 2.1 SNAPSHOT demo database is deployed. The main
advantage of using standard images is that the updates to those are automatically
available. The lh-toolkit.war is downloaded from the latest devbuilds that are
uploaded to bintray, using Gitlab CI. Thus, the webapplication built from the
master branch is available as part of this container. You only have to update
the image and get the latest and greatest lh-toolkit that is available from the
Continuous Integration.

## Running the container
If you are using Windows, please refer to our [README for Windows](README_windows.md).

### Installing Docker on Fedora
To install Docker, you need the 64-bit version of one of these Fedora versions:
- 28
- 29

First, uninstall any older version of Docker using the following command:

```
sudo dnf remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-selinux \
                  docker-engine-selinux \
                  docker-engine
```

The recommended approach of installing Docker CE by setting up Docker's repositories has been described here. For other approaches, please refer to [Docker Docs](https://docs.docker.com/install/linux/docker-ce/fedora).

#### Set up the repository

First, install the `dnf-plugins-core` package:

```
sudo dnf -y install dnf-plugins-core
```

![](images/linux/install-dnf-plugins-core-fedora.png)

Next, set up the stable repository:

```
sudo dnf config-manager \
    --add-repo \
    https://download.docker.com/linux/fedora/docker-ce.repo
```

![](images/linux/add-docker-repo-fedora.png)

#### Install Docker-CE

Install the latest version of Docker CE using the following command:

```
sudo dnf install docker-ce docker-ce-cli containerd.io
```

Press **`y`** when prompted. If prompted to accept the GPG key, verify that the fingerprint matches **`060A 61C5 1B55 8A7F 742B 77AA C52F EB6B 621E 9F35`**, and if so, accept it.


Now, start docker using the following command:

```
sudo systemctl start docker
```

To verify that Docker has been installed succesfully, check the Docker version.

```
docker --version
```

The output on terminal should be similar to this:

```
Docker version 18.09.3, build 774a1f4
```

![](images/linux/docker-version-fedora.png)

#### Install Docker Compose

To install Docker Compose, download the current stable release (1.24.0-rc1).

```
sudo curl -L https://github.com/docker/compose/releases/download/1.24.0-rc1/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
```

Next, apply executable permissions to the binary.

```
sudo chmod +x /usr/local/bin/docker-compose
```

### Installing Docker on Ubuntu

To install Docker CE, you need the 64-bit version of one of these versions:

- Cosmic 18.10
- Bionic 18.04 (LTS)
- Xenial 16.04 (LTS)

First, uninstall the older docker versions (if any).

```
sudo apt-get remove docker docker-engine docker.io containerd runc
``` 

The recommended approach of installing Docker CE by setting up Docker's repositories has been described here. For other approaches, please refer to [Docker Docs](https://docs.docker.com/install/linux/docker-ce/fedora).

#### Set up the repository

First, update the `apt` package index:

```
sudo apt-get update
```

Install packages to allow `apt` to use a repository over HTTPS:

```
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
```

Press **`y`** when prompted.

![](images/linux/apt-get-install-packages-ubuntu.png)

Add Docker's official GPG key:

```
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```

Next, set up the stable repository:

```
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
```

#### Install Docker-CE

First, update the `apt` package index.

```
sudo apt-get update
```

Next, install the latest version of Docker CE.

```
sudo apt-get install docker-ce docker-ce-cli containerd.io
```

![](images/linux/apt-get-docker-ce-ubuntu.png)

To verify that Docker has been installed succesfully, check the Docker version.

```
docker --version
```

The output on terminal should be similar to this:

```
Docker version 18.09.3, build 774a1f4
```

![](images/linux/docker-version-ubuntu.png)

#### Install Docker Compose

To install Docker Compose, download the current stable release (1.24.0-rc1).

```
sudo curl -L https://github.com/docker/compose/releases/download/1.24.0-rc1/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
```

![](images/linux/docker-compose-install-ubuntu.png)

Next, apply executable permissions to the binary.

```
sudo chmod +x /usr/local/bin/docker-compose
```

### Pulling the container image
To run containers pull the container image:
```
docker pull registry.gitlab.com/librehealth/toolkit/lh-toolkit-docker:latest
```
You should see the similar console output:

![](images/linux/pulling_docker_img.png)

Navigate to the directory where you cloned this project. Depending on how you want to interact with the container, run it in foreground or as a daemon.

**Ensure that you do not have any other servers running on 8080 (tomcat) and 3306 (mysql).**

To run the container in the foreground:
```
docker-compose -f docker-compose.dev.yml up
```
![](images/linux/running_container_foreground.png)

MySQL will be started first and then lh-toolkit will be started on the containers.
When you are done using lh-toolkit you can press `Ctrl+C` to stop the container.

To run the container in the background:
```
docker-compose -f docker-compose.dev.yml up -d
```
![](images/linux/running_container_background.png)

## Using lh-toolkit
To start using lh-toolkit, point your browser to localhost:8080/lh-toolkit .
The following are the authentication information:

* **User**: admin
* **Pass**: Admin123

![](images/linux/login_screen.png)

## Bringing container down
To bring the container down and to free space on your machine run:
```
docker-compose down
```
![](images/linux/bringing_container_down.png)

## Troubleshooting
When you are pulling the container image, the directory you are in does not matter.
However, if you try to run this docker image from outside of this project directory, you will get the following error:

![](images/linux/running_image_from_wrong_dir.png)

Navigate to the project directory and enter the command again.
